import { Component, OnInit, Output, EventEmitter }  from '@angular/core';
import { Router }                                   from '@angular/router';
import { SharedService }                            from '../../services/shared.service';
import { RegisterService }                          from '../../services/register.service';

@Component({
  selector: 'app-register',
  templateUrl: './register.component.html',
  styleUrls: ['./register.component.css']
})
export class RegisterComponent implements OnInit {

  email: string;
  password: string;
  retyped: string;

  isSuccessedRegister: boolean;

  @Output() Register: EventEmitter<any> = new EventEmitter();

  constructor(
    private router: Router,
    private _sharedService: SharedService,
    private registerService: RegisterService
  ) { }

  ngOnInit() {
    this.email = this.password = this.retyped = "";
  }
  
  RegisterForm(e) {
    if (this.password !== this.retyped) {
      alert("Retyped is not correct");
    } else {
      this.registerService.register(this.email, this.password, this.retyped)
        .subscribe (
          res => { 
            if (res && res.token) {
              localStorage.setItem('currentUser', JSON.stringify(res));
              this._sharedService.emitChange(this.email);
            }
            this.router.navigate(['/']);
          },
          err => console.log(err)
        );
    }
  }
}