import { NgModule }         from '@angular/core'
import { FormsModule }      from '@angular/forms';
import { CommonModule }     from '@angular/common';
import { BrowserModule }    from '@angular/platform-browser';
import { HeaderComponent }  from '../header/header.component';

@NgModule({
  imports:[
    BrowserModule,
    FormsModule,
    CommonModule
  ],
  declarations:[
    HeaderComponent
  ],
  exports: [
    HeaderComponent
  ]
})

export class HeaderModule {}