import { Component, OnInit }    from '@angular/core';
import { Router }               from '@angular/router';
import { SharedService }        from '../../services/shared.service';
import { LoginService }         from '../../services/login.service';

@Component({
  selector: 'app-login',
  templateUrl: './login.component.html',
  styleUrls: ['./login.component.css']
})
export class LoginComponent implements OnInit {
  email: string;
  password: string;
  constructor(
    private router: Router,
    private loginService: LoginService,
    private _sharedService: SharedService
  ) { }

  ngOnInit() {
    this.email = this.password = "";
  }

  LoginForm(e) {
    this.loginService.login(this.email, this.password)
        .subscribe (
          res => { 
            if (res && res.token) {
              localStorage.setItem('currentUser', JSON.stringify(res));
              console.log("dang nhap thanh cong");
              this._sharedService.emitChange(this.email);
            }
          },
          err => {
            this.router.navigate(['/login']);
            console.log(err);
          }
        );
  }
}