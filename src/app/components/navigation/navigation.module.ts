import { NgModule }                   from '@angular/core'
import { FormsModule }                from '@angular/forms';
import { CommonModule }               from '@angular/common';
import { BrowserModule }              from '@angular/platform-browser';
import { RegisterComponent }          from '../register/register.component';
import { LoginComponent }             from '../login/login.component';
import { NavigationRoutingModule }    from './navigation.routing';
import { HeaderModule }               from '../header/header.module';
import { HomepageModule }             from '../homepage/homepage.module';

@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    NavigationRoutingModule,
    HeaderModule,
    HomepageModule
  ],
  declarations:[
    RegisterComponent,
    LoginComponent
  ]
})

export class NavigationModule {}