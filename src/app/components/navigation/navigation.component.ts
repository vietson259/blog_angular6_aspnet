import { Component, OnInit, OnChanges, SimpleChange } from '@angular/core';
import { NavigationClass } from './navigation';
import { AppService } from '../../services/app.service';
import { Router } from '@angular/router';
import { SharedService } from '../../services/shared.service';
import { AuthService } from '../../services/auth.service';

@Component({
  selector: 'app-navigation',
  templateUrl: './navigation.component.html',
  styleUrls: ['./navigation.component.css'],
  providers: [SharedService]
})
export class NavigationComponent implements OnInit {

  status: NavigationClass = new NavigationClass();

  constructor (
    private router: Router,
    private appService: AppService,
    private authService: AuthService,
    private _sharedService: SharedService
  ) {
  }

  ngOnInit() {
    if (this.appService.isLogged()) {
      this.status = this.appService.updateSTATUS();
    }
    
    this._sharedService.changeEmitted$.subscribe(
      event => {
          this.status.author = event;
          this.status.status = true;
          this.checkLogin(event);
      });
  }

  private checkLogin(e) {
    if (this.appService.isLogged()) {
      this.appService.updateSTATUS();
      console.log(this.authService.redirectUrl);
      if (this.authService.redirectUrl == undefined) this.authService.redirectUrl = "/";
      if (this.authService.redirectUrl == '/homeblog/') 
        this.authService.redirectUrl += this.status.author;
        
      this.authService.isLoggedIn = true;
      this.router.navigate([this.authService.redirectUrl]);
    } else {
      this.router.navigate(['/login']);
    }
  }

  logout(e) {
    localStorage.removeItem('currentUser');
    this.status.status = this.authService.isLoggedIn = false;
    this.status.author = "";
    this.appService.STATUS = this.status;
    this.router.navigate(['/']);
  }
}