import { NgModule }             from "@angular/core";
import { RouterModule }         from "@angular/router";
import { HomepageComponent }    from "../homepage/homepage.component";
import { LoginComponent }       from "../login/login.component";
import { RegisterComponent }    from "../register/register.component";

const navigationRoutes = [
    {   path: '' , 
        children: [
            { path: '', component: HomepageComponent },
            { path: 'login', component: LoginComponent },
            { path: 'register', component: RegisterComponent }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(navigationRoutes)],
    exports: [RouterModule]
})

export class NavigationRoutingModule {}