import { Component, OnInit }        from '@angular/core';
import { AppService }               from '../../services/app.service';
import { ActivatedRoute, Router }   from '../../../../node_modules/@angular/router';
import { HomeblogService }          from '../../services/homeblog.service';

@Component({
  selector: 'app-homeblog',
  templateUrl: './homeblog.component.html',
  styleUrls: ['./homeblog.component.css']
})
export class HomeblogComponent implements OnInit {

  posts;
  username: string;
  
  constructor (
    private appService: AppService,
    private homeblogService: HomeblogService,
    private activatedRouter: ActivatedRoute,
    private router: Router
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.activatedRouter.paramMap.pipe().subscribe(
      res => {
        this.username = res.get("author");
        console.log(this.username);
        this.homeblogService.getBlogDetail(this.username)
          .subscribe(
            result => {
              this.posts = result;
              console.log(this.posts);
            },
            err => { 
              console.log(err);
              this.appService.catchError401(err);
            }
          )
      },
      err => this.router.navigate['/']
    );
  }

  editEvent(e) {
    this.router.navigate([`/homeblog/${this.username}/edit`, e.toElement.id]);
  }

  deleteEvent(e) {
    this.homeblogService.deletePost(e.toElement.id)
      .subscribe(res => {
        alert("delete post successly");
        this.posts = this.posts.filter(p => p.idPost != e.toElement.id);
      }, err => {
        this.appService.catchError401(err);
        alert("delete post unsuccessly");
        console.log(err);
      });
  }
}
