import { NgModule }             from "@angular/core";
import { RouterModule }         from "@angular/router";
import { HomeblogComponent }    from "./homeblog.component";
import { AuthGuard }            from "../../services/auth-guard.service";
import { EditBlogComponent }    from "../homepage/components/edit-blog/edit-blog.component";
import { EditorComponent }      from "../editor/editor.component";

const homeblogRoutes = [
    { path: '', 
        children: [
            { path: '', component: HomeblogComponent },
            { path: 'edit/:idPost', component: EditBlogComponent, canActivate: [AuthGuard] },
            { path: 'editor', component: EditorComponent, canActivate: [AuthGuard] }
        ]
    }
];

@NgModule({
    imports: [RouterModule.forChild(homeblogRoutes)],
    exports: [RouterModule]
})

export class HomeblogRoutingModule {}