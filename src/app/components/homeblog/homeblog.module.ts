import { NgModule }               from '@angular/core'
import { HomeblogComponent }      from './homeblog.component';
import { HomeblogRoutingModule }  from './homeblog.routing';
import { EditorModule }           from '../editor/editor.module';
import { CommonModule }           from '@angular/common';
import { EditBlogModule } from '../homepage/components/edit-blog/edit-blog.module';

@NgModule({
  imports:[
    CommonModule,
    HomeblogRoutingModule,
    EditorModule,
    EditBlogModule
  ],
  declarations:[
    HomeblogComponent
  ]
})

export class HomeblogModule {}