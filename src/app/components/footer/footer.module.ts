import { NgModule }       from '@angular/core'
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';
import { FooterComponent } from './footer.component';

@NgModule({
  imports:[
    BrowserModule,
    FormsModule,
    CommonModule
  ],
  declarations:[
    FooterComponent
  ],
  exports: [
    FooterComponent
  ]
})

export class FooterModule {}