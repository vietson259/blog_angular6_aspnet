import { NgModule }       from '@angular/core'
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';
import { EditorComponent } from './editor.component';
import { FroalaEditorModule, FroalaViewModule } from '../../../../node_modules/angular-froala-wysiwyg';

@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    FroalaEditorModule.forRoot(), 
    FroalaViewModule.forRoot()
  ],
  declarations:[
    EditorComponent
  ],
  exports: [
    EditorComponent
  ]
})

export class EditorModule {}