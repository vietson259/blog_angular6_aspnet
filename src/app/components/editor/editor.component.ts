import { Component, OnInit, Input } from '@angular/core';
import { AppService }               from '../../services/app.service';
import { Router }                   from '@angular/router';
import { EditorService }            from '../../services/editor.service';

@Component({
  selector: 'app-editor',
  templateUrl: './editor.component.html',
  styleUrls: ['./editor.component.css']
})
export class EditorComponent implements OnInit {
  @Input() oldContent;
  @Input() id;

  author: string;
  content: string;
  title: string;
  mainContent: string;
  token: string;

  constructor(
    private appService: AppService,
    private router: Router,
    private editorService: EditorService
  ) { }
  
  ngOnInit() {
    if (this.oldContent == undefined) {
      this.content = this.title = this.mainContent = this.id = "";
    } else {
      this.content = this.oldContent.contentPost;
      this.title = this.oldContent.title;
      this.mainContent = this.oldContent.mainContent;
    }

    this.author = this.appService.getCurrentUser();
    if (this.author == "") {
      this.router.navigate(["/login"]);
    }

    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      this.token = currentUser.token;
    }
  }

  postEvent(e) {
    this.editorService.post(this.title, this.mainContent, this.content, this.author)
        .subscribe(
          res => {
            alert("post successly");
            this.router.navigate(["/homeblog", this.author]);
          },
          err => {
            alert("post unsuccess");
            console.log(err);
          }
        );
  }

  cancelEvent(e) {
    this.title = this.mainContent = this.content = "";
  }

  updateEvent(e) {
    this.editorService.edit(this.id, this.title, this.mainContent, this.content, this.author)
        .subscribe(
          res => {
            alert("edit successly");
            this.router.navigate(["/homeblog", this.author]);
          },
          err => {
            alert("edit unsuccess");
            console.log(err);
          }
        );
  }

  public options: Object = {
    requestHeaders: {
      Authorization: `Bearer ${this.token}`
    },

    charCounterCount: true,
    // Set the image upload parameter.
    imageUploadParam: 'image_param',

    // Set the image upload URL.
    imageUploadURL: 'https://localhost:44332/api/values/',

    // Additional upload params.
    imageUploadParams: {id: 'my_editor'},

    // Set request type.
    imageUploadMethod: 'POST',

    // Set max image size to 5MB.
    imageMaxSize: 5 * 1024 * 1024,

    // Allow to upload PNG and JPG.
    imageAllowedTypes: ['jpeg', 'jpg', 'png']
  }
}