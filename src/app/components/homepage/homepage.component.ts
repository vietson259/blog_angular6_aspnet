import { Component, OnInit } from '@angular/core';
import { HomepageService } from '../../services/homepage.service';
import { BlogDetail } from '../../services/blog.service';

@Component({
  selector: 'app-homepage',
  templateUrl: './homepage.component.html',
  styleUrls: ['./homepage.component.css']
})

export class HomepageComponent implements OnInit {
  blogDetailClasses: BlogDetail[];
  pagedNumber: number;
  currentPage: number;
  limit = 3;
  blogsDisplay;
  page: number;
  isNextValid: boolean;
  isBackValid: boolean;
  constructor(
    private homepageService: HomepageService
  ) { }

  ngOnInit() {
    this.pagedNumber = this.page = 0;
    this.currentPage = 1;
    this.isBackValid = false; this.isNextValid = true;
    this.getBlogs();
  }

  getBlogs() {
    this.homepageService.getDataHomepageBlog(this.pagedNumber)
      .subscribe(
        res => {
          this.blogDetailClasses = res;
          this.pagedNumber = Math.ceil(this.blogDetailClasses.length / this.limit);
          this.getBlogsDisplay(this.currentPage);
          if (this.pagedNumber == 1) this.isNextValid = false;
        },
        err => console.log(err)
      )
  }

  private getBlogsDisplay(index) {
    this.blogsDisplay = [];
    let begin = ((index - 1) * this.limit);
    if (this.blogDetailClasses[begin] == undefined) return true;
    let end = begin + this.limit - 1;
    while (begin <= end && begin < this.blogDetailClasses.length) {
      this.blogsDisplay.push(this.blogDetailClasses[begin]);
      begin++;
    }
  }

  backClick(e) {
    if (this.currentPage > 1) {
      this.getBlogsDisplay(--this.currentPage);
      this.isNextValid = true;
      if (this.currentPage == 1) this.isBackValid = false;
    }
  }

  nextClick(e) {
    if (this.currentPage < this.pagedNumber) {
      this.getBlogsDisplay(++this.currentPage);
      this.isBackValid = true;
    } else {
      this.callPlusData();
    }
  }

  private callPlusData() {
    this.page++;
    this.homepageService.getDataHomepageBlog(this.page)
      .subscribe(
        res => {
          if (res == []) {
            this.page--;
            this.isNextValid = false;
          } else {
            this.currentPage++;
            res.forEach(element => {
              this.blogDetailClasses.push(element);
            });
            this.pagedNumber = Math.ceil(this.blogDetailClasses.length / 3);
            this.getBlogsDisplay(this.currentPage);
            if (this.getBlogsDisplay.length < this.limit) this.isNextValid = false;
          }
        },
        err => console.log(err)
      )
  }
}