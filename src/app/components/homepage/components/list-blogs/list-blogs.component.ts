import { Component, OnInit, Input, Output, EventEmitter }  from '@angular/core';

@Component({
  selector: 'app-list-blogs',
  templateUrl: './list-blogs.component.html',
  styleUrls: ['./list-blogs.component.css']
})
export class ListBlogsComponent implements OnInit{

  @Input() blogsDisplay;
  @Input() isNextValid;
  @Input() isBackValid;

  @Output() next = new EventEmitter();
  @Output() back = new EventEmitter();

  totalPage: number;

  limit: number = 3;

  constructor (
    
  ) { }

  ngOnInit() {
  }

  nextClick(e) {
    this.next.emit(e);
  }

  backClick(e) {
    this.back.emit(e);
  }
}