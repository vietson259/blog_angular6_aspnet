import { Component, OnInit, Input, Output, EventEmitter } from '@angular/core';

@Component({
  selector: 'app-paged-list',
  templateUrl: './paged-list.component.html',
  styleUrls: ['./paged-list.component.css']
})
export class PagedListComponent implements OnInit {

  @Input() isNextValid;
  @Input() isBackValid;
  @Output() next = new EventEmitter();
  @Output() back = new EventEmitter();

  constructor(

  ) { }

  ngOnInit() {}

  nextClick(e) {
    this.next.emit(e);
  }

  backClick(e) {
    this.back.emit(e);
  }
}