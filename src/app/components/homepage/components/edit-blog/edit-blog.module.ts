import { NgModule }           from '@angular/core'
import { FormsModule }        from '@angular/forms';
import { BrowserModule }      from '@angular/platform-browser';
import { EditBlogComponent }  from './edit-blog.component';
import { EditorModule }       from '../../../editor/editor.module';
import { CommonModule }       from '@angular/common';

@NgModule({
  imports:[
    CommonModule,
    FormsModule,
    EditorModule
  ],
  declarations:[
    EditBlogComponent
  ],
  exports: [
    EditBlogComponent
  ]
})

export class EditBlogModule {}