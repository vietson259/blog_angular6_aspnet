import { Component, OnInit, Input }   from '@angular/core';
import { ActivatedRoute }             from '@angular/router';
import { BlogService }                from '../../../../services/blog.service';

@Component({
  selector: 'app-edit-blog',
  templateUrl: './edit-blog.component.html',
  styleUrls: ['./edit-blog.component.css']
})
export class EditBlogComponent implements OnInit {

  idPost: number;
  data;

  constructor(
    private activatedRouter: ActivatedRoute,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.idPost = +this.activatedRouter.snapshot.paramMap.get('idPost');
    this.getData(this.idPost);
  }

  getData(id: number) {
    this.blogService.getPostDetail(id)
          .subscribe(
            result => {
              this.data = result;
              console.log(this.data);
            },
            err => console.log(err)
          );
  }
}
