import { NgModule }               from '@angular/core'
import { FormsModule }            from '@angular/forms';
import { CommonModule }           from '@angular/common';
import { BrowserModule }          from '@angular/platform-browser';
import { SideBarComponent }       from '../side-bar/side-bar.component';
import { ListBlogsComponent }     from './components/list-blogs/list-blogs.component';
import { HomepageRoutingModule }  from './homepage-routing.module';
import { PagedListComponent }     from './components/list-blogs/components/paged-list/paged-list.component';
import { HomepageComponent }      from './homepage.component';
import { HeaderModule }           from '../header/header.module';
import { FooterModule }           from '../footer/footer.module';

@NgModule({
  imports:[
    CommonModule,
    HomepageRoutingModule,
    HeaderModule,
    FooterModule
  ],
  declarations:[
    HomepageComponent,
    SideBarComponent,
    ListBlogsComponent,
    PagedListComponent
  ]
})

export class HomepageModule {}