import { NgModule } from "@angular/core";
import { RouterModule } from "@angular/router";

const homepageRoutes = [

];

@NgModule({
    imports: [RouterModule.forChild(homepageRoutes)],
    exports: [RouterModule]
})

export class HomepageRoutingModule {}