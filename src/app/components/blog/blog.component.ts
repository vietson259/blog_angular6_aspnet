import { Component, OnInit }    from '@angular/core';
import { ActivatedRoute }       from '../../../../node_modules/@angular/router';
import { BlogService }          from '../../services/blog.service';

@Component({
  selector: 'app-blog',
  templateUrl: './blog.component.html',
  styleUrls: ['./blog.component.css']
})
export class BlogComponent implements OnInit {

  data;

  id : number = 0;

  constructor (
    private activatedRouter: ActivatedRoute,
    private blogService: BlogService
  ) { }

  ngOnInit() {
    this.getData();
  }

  getData() {
    this.activatedRouter.paramMap.pipe().subscribe(
      res => {
        this.id = parseInt(res.get("idPost"));
        this.blogService.getPostDetail(this.id)
          .subscribe(
            result => {
              this.data = result;
              console.log(this.data);
            },
            err => console.log(err)
          );
      }
    );
  }

  postCommentEvent(e) {
    console.log(e);
  }

}