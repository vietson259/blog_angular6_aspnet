import { NgModule }       from '@angular/core'
import { FormsModule }    from '@angular/forms';
import { CommonModule }   from '@angular/common';
import { BrowserModule }  from '@angular/platform-browser';
import { BlogComponent } from './blog.component';
import { HeaderModule } from '../header/header.module';
import { CommentComponent } from './components/comment/comment.component';
import { ContentComponent } from './components/content/content.component';
import { RecentPostComponent } from './components/recent-post/recent-post.component';

@NgModule({
  imports:[
    BrowserModule,
    FormsModule,
    CommonModule,
    HeaderModule
  ],
  declarations:[
    BlogComponent,
    CommentComponent,
    ContentComponent,
    RecentPostComponent
  ],
  exports: [
    BlogComponent
  ]
})

export class BlogModule {}