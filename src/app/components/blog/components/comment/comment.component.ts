import { Component, OnInit, Input}  from '@angular/core';
import { CommentService }           from '../../../../services/comment.service';

@Component({
  selector: 'app-comment',
  templateUrl: './comment.component.html',
  styleUrls: ['./comment.component.css']
})
export class CommentComponent implements OnInit {

  @Input() comments;
  @Input() idBlog;

  comment: string;

  constructor(
    private commentService: CommentService
  ) { }

  ngOnInit() {
    this.comment = "";
    if (this.comments == undefined) return;
  }

  commentEvent(e) {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.user) {
      currentUser = currentUser.user;
    } else {
      currentUser = "anonymous";
    }
    console.log(this.comment + " " + this.idBlog + currentUser.user);
    if (this.comment == "") {
      alert("please type your comment!");
      return;
    } 
    this.commentService.postComment(this.idBlog, this.comment, currentUser)
      .subscribe(
        res => {console.log(res);
          if (res == "200") {
            alert("post comment is success");
            this.comments.comments.unshift({author: currentUser, content: this.comment});
            this.comment = "";
          }
        },
        err => alert(err)
      );
  }

  cancelEvent(e) {
    console.log(e);
    this.comment = "";
  }
}