import { Component, OnInit, Input } from '@angular/core';
import { Router }                   from '../../../../../../node_modules/@angular/router';

@Component({
  selector: 'app-recent-post',
  templateUrl: './recent-post.component.html',
  styleUrls: ['./recent-post.component.css']
})
export class RecentPostComponent implements OnInit {

  @Input() recentPosts;
  
  constructor(
    private router: Router
  ) { }

  ngOnInit() {
    if (this.recentPosts == undefined) return ;
  }

  onClick(e) {
    this.router.navigate(['blog/' +  e.toElement.id + '/' + e.toElement.name]);
  }

}
