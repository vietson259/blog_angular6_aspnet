import { Component, OnInit, Input }   from '@angular/core';
import { CContent }                   from '../../../../services/blog.service';

@Component({
  selector: 'app-content',
  templateUrl: './content.component.html',
  styleUrls: ['./content.component.css']
})
export class ContentComponent implements OnInit {
  @Input() content: CContent;
  
  constructor() { }

  ngOnInit() {
    if (this.content == undefined) return;
    console.log(this.content);
  }

}
