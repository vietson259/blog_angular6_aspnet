import { Component, OnInit }   from '@angular/core';
import { AppService }          from './services/app.service';
import { NavigationClass }     from './components/navigation/navigation';

@Component({
  selector: 'app-root',
  templateUrl: './app.component.html',
  styleUrls: ['./app.component.css']
})
export class AppComponent implements OnInit {
  navigationClass: NavigationClass;

  constructor (
    private appService: AppService    
  ){}

  ngOnInit() {

  }

  getStateLogin() {
    this.appService.getStateLogin()
      .subscribe(
        res => {
          this.navigationClass = res;
          console.log(res);
        },
        err => console.log(err)
      );
  }
}