import { NgModule }                 from '@angular/core';
import { RouterModule, Routes }     from '@angular/router';
import { Page404Component }         from './components/page404/page404.component';
import { BlogComponent }            from './components/blog/blog.component';
//import { CanDeactivateGuard }       from './services/can-deactivate-guard.service';
import { AuthGuard }                from './services/auth-guard.service';
import { NavigationModule }         from './components/navigation/navigation.module';
import { HomeblogModule }           from './components/homeblog/homeblog.module';
import { SelectivePreloadingStrategy } from './services/selective-preloading-strategy';

const appRoutes: Routes = [
  { path: '', redirectTo: 'navigation', pathMatch: 'full'},
  { path: 'navigation', 
    loadChildren: './components/navigation/navigation.module#NavigationModule' },
  { path: 'homeblog/:author',
    loadChildren: './components/homeblog/homeblog.module#HomeblogModule', 
    data: { preload: true },
    canActivate: [AuthGuard] },
  { path: 'blog/:idPost/:slug', component:  BlogComponent }, 
  { path: '**', component: Page404Component }
];

@NgModule({
  imports: [
    RouterModule.forRoot(
      appRoutes, {
        preloadingStrategy: SelectivePreloadingStrategy,
      }
    )
  ],
  exports: [
    RouterModule,
    NavigationModule
  ],
  providers: [
    //CanDeactivateGuard,
    AuthGuard,
    SelectivePreloadingStrategy
  ]
})
export class AppRoutingModule { }