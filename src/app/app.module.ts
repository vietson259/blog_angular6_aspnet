import { NgModule }           from '@angular/core';
import { HttpClientModule }   from '@angular/common/http';

import { Page404Component }   from './components/page404/page404.component';
import { AppRoutingModule }   from './app-routing.module';
import { AuthGuard }          from './services/auth-guard.service';
import { AppService }         from './services/app.service';
import { AppComponent }       from './app.component';
import { AuthService }        from './services/auth.service';
import { BlogModule }         from './components/blog/blog.module';
import { NavigationModule }   from './components/navigation/navigation.module';
import { NavigationComponent }from './components/navigation/navigation.component';
import { BrowserModule }      from '../../node_modules/@angular/platform-browser';
import { FormsModule }        from '../../node_modules/@angular/forms';

@NgModule({
  declarations: [
    AppComponent,
    Page404Component,
    NavigationComponent
  ],
  imports: [
    BrowserModule,
    FormsModule,
    HttpClientModule,
    AppRoutingModule,
    NavigationModule,
    BlogModule
  ],
  providers: [
    AuthGuard,
    AppService,
    AuthService
  ],
  bootstrap: [AppComponent]
})
export class AppModule { 
  constructor() {
    
  }
}