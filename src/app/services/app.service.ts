import { Injectable, OnInit }             from '@angular/core';
import { HttpErrorResponse }              from '@angular/common/http';
import { Observable, throwError, of }     from 'rxjs';
import { NavigationClass }                from '../components/navigation/navigation';
import { Router }                         from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class AppService implements OnInit{

  STATUS: NavigationClass = {
    status: false,
    author: "anonymous"
  }

  constructor (
    private router: Router
  ) { }

  ngOnInit() {
  }

  getStateLogin(): Observable<NavigationClass>{
    return of(this.STATUS);
  }

  handleError(error: HttpErrorResponse) {
    if (error.error instanceof ErrorEvent) {
      // A client-side or network error occurred. Handle it accordingly.
      console.error('An error occurred:', error.error.message);
    } else {
      // The backend returned an unsuccessful response code.
      // The response body may contain clues as to what went wrong,
      console.error(
        `Backend returned code ${error.status}, ` +
        `body was: ${error.error}`);
    }
    // return an ErrorObservable with a user-facing error message
    return throwError(
      'Something bad happened; please try again later.');
  }

  getHttpHeadersContent() {
    let headers = {"Content-Type" : "application/json; charset=utf-8;", "Authorization": ""};
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.token) {
      headers['Authorization'] = `Bearer ${currentUser.token}`;
    }
    return headers;
  }

  getCurrentUser() : string {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.user) {
      return currentUser.user;
    } 
    return "";
  }

  updateSTATUS() {
    let currentUser = JSON.parse(localStorage.getItem('currentUser'));
    if (currentUser && currentUser.user) {
      this.STATUS.author = currentUser.user;
      this.STATUS.status = true;
    } 
    return this.STATUS;
  }

  isLogged() {
    if (this.getCurrentUser() == "") return false;
    return true;
  }

  catchError401(err) {
    console.log(err['status']);
    if (err['status'] == 401) {
      this.router.navigate(['/login']);
    }
  }
}