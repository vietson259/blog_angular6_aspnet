import { Injectable }     from '@angular/core';
import { AppService }     from './app.service';
import { environment }    from '../../environments/environment.prod';
import { HttpClient }     from '@angular/common/http';
import { catchError }     from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class EditorService {

  constructor(
    private appService: AppService,
    private http: HttpClient
  ) { }

  post(title, mainContent, content, author) {
    let url = environment.apiBlogUrl + 'blog/post';
    let headers = {headers: this.appService.getHttpHeadersContent()};
    let payload = {Title: title, MainContent: mainContent, Content: content, Username: author};
    return this.http.post(url, payload, headers).pipe(
      catchError( this.appService.handleError )
    );
  }

  edit(id, title, mainContent, content, author) {
    let url = environment.apiBlogUrl + 'blog/edit';
    let headers = {headers: this.appService.getHttpHeadersContent()};
    let payload = {ID: id, Title: title, MainContent: mainContent, Content: content, Username: author};
    return this.http.put(url, payload, headers).pipe(
      catchError( this.appService.handleError )
    );
  }
}
