import { Injectable, OnInit }   from '@angular/core';
import { AppService }           from './app.service';

@Injectable()
export class AuthService implements OnInit {
  constructor(
    private appService: AppService
  ) {}

  isLoggedIn = false;
  redirectUrl: string;

  ngOnInit() {
    this.redirectUrl = "/";
    if (this.appService.isLogged()) {
      this.isLoggedIn = true;
    }
  }
}