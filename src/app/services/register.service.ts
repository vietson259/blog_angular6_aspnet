import { Injectable }     from '@angular/core';
import { environment }    from '../../environments/environment.prod';
import { AppService }     from './app.service';
import { HttpClient }     from '@angular/common/http';
import { Observable }     from 'rxjs';
import {catchError}       from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class RegisterService {

  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  public register(email: string, password: string, retyped: string): Observable<any> {
    let url = environment.apiBlogUrl + 'account/register';
    let payload = {Email: email, PassWord: password, Retyped: retyped};
    let headers = {headers: this.appService.getHttpHeadersContent()}
    return this.http.post(url, payload, headers)
      .pipe(
        catchError( this.appService.handleError )
      );
  }
}