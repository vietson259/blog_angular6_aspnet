import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { AppService }   from './app.service';
import { environment }  from '../../environments/environment.prod';
import { Observable }   from 'rxjs';
import { catchError }   from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomepageService {

  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  getDataHomepageBlog(pagedNumber): Observable<any>{
    let url = environment.apiBlogUrl + 'blog/getListAllPosts/' + pagedNumber;
    let headers = {headers: this.appService.getHttpHeadersContent()};
    return this.http.get(url, headers).pipe(
      catchError( this.appService.handleError )
    );
  }
}