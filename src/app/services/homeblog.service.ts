import { Injectable }   from '@angular/core';
import { HttpClient }   from '@angular/common/http';
import { AppService }   from './app.service';
import { environment }  from '../../environments/environment.prod';
import { catchError }   from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class HomeblogService {

  constructor(
    private appService: AppService,
    private http: HttpClient
  ) { }

  public getBlogDetail (username: string) {
    let url = environment.apiBlogUrl + 'blog/getBlogDetail/' + username;
    let option = {
      headers: this.appService.getHttpHeadersContent()
    }
    return this.http.get(url, option);
  }

  public deletePost (idPost) {
    let url = environment.apiBlogUrl + 'blog/deletePost/' + idPost;
    console.log(url);
    let option = {
      headers: this.appService.getHttpHeadersContent()
    }
    return this.http.delete(url, option).pipe(
      catchError( this.appService.handleError )
    );
  }
}
