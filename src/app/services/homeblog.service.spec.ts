import { TestBed, inject } from '@angular/core/testing';

import { HomeblogService } from './homeblog.service';

describe('HomeblogService', () => {
  beforeEach(() => {
    TestBed.configureTestingModule({
      providers: [HomeblogService]
    });
  });

  it('should be created', inject([HomeblogService], (service: HomeblogService) => {
    expect(service).toBeTruthy();
  }));
});
