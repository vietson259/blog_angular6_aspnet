import { Injectable }   from '@angular/core';
import { environment }  from '../../environments/environment.prod';
import { AppService }   from './app.service';
import { HttpClient }   from '@angular/common/http';
import { catchError }   from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class CommentService {

  constructor(
    private appService: AppService,
    private http: HttpClient,
  ) { }
  
  postComment(id, content, author) {
    let url = environment.apiBlogUrl + 'blog/comment';
    let payload = {PostID: id, Content: content, AuthorName: author};
    let headers = {headers: this.appService.getHttpHeadersContent()}
    return this.http.post(url, payload, headers).pipe(
      catchError( this.appService.handleError )
    );
  }
}

export class CCommentDetail {
  author: string;
  content: string;
}
