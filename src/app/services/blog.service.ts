import { Injectable }   from '@angular/core';
import { environment }  from '../../environments/environment.prod';
import { HttpClient }   from '@angular/common/http';
import { AppService }   from './app.service';
import { catchError }   from 'rxjs/operators';

@Injectable({
  providedIn: 'root'
})
export class BlogService {

  constructor(
    private http: HttpClient,
    private appService: AppService
  ) { }

  getPostDetail(id) {
    let url = environment.apiBlogUrl + 'blog/getPostDetail/' + id;
    let option = {
      headers: this.appService.getHttpHeadersContent()
    }
    return this.http.get(url, option).pipe(
      catchError( this.appService.handleError )
    );
  }
}

export class CContent {
  content: string;
}

export class CRecentPost {
  title: string;
  id: string;
}

export class BlogDetail {
  id: string;
  idAuthor: string;
  title: string;
  mainContent: string;
  author: string;
  avatar: string;
  date: string;
  slug: string;
}