export const environment = {
  production: true,
  pokemonUrl: 'https://pokeapi.co/api/v2/pokemon/',
  apiBlogUrl: 'https://localhost:44332/api/'
};