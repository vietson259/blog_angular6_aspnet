﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.EntityFrameworkCore;

namespace MvcBlog.Models
{
    public class MvcBlogContext : DbContext
    {
        public MvcBlogContext (DbContextOptions<MvcBlogContext> options)
            : base(options)
        {
        }

        public DbSet<MvcBlog.Models.Movie> Movie { get; set; }
        public DbSet<CAccount> Account { get; set; }
        public DbSet<CComment> Comment { get; set; }
        public DbSet<CInfomation> Infomation { get; set; }
        public DbSet<CPost> Post { get; set; }
    }
}
