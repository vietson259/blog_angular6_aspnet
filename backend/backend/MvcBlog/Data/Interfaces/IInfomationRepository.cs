﻿using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.Interfaces
{
    public interface IInfomationRepository
    {
        CInfomation Infomations { get; }
        CInfomation GetInfomationByAccountID(int AccountID);
    }
}
