﻿using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.Interfaces
{
    public interface ICommentRepository
    {
        IEnumerable<CComment> Comments { get; }
    }
}
