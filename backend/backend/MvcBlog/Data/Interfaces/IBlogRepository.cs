﻿using MvcBlog.Data.ViewModel;
using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.Interfaces
{
    public interface IBlogRepository
    {
        IEnumerable<PostDetailViewModel> GetListAllPosts(int page);
        PostDetailCellViewModel GetBlogDetailCellByIDPost(int id);
        IEnumerable<BlogDetailViewModel> GetBlogDetailViewModelsByUserName(string username);
        void AddComment(CComment comment);
        void DeleteBlog(string id);
        void CreatePost(PostViewModel post);
        void EditPost(PostViewModel post);
    }
}