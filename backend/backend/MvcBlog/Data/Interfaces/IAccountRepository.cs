﻿using MvcBlog.Models;
using System.Collections.Generic;
using System.Threading.Tasks;
using MvcBlog.Data.ViewModel;
using Microsoft.AspNetCore.Mvc;

namespace MvcBlog.Data.Interfaces
{
    public interface IAccountRepository
    {
        IActionResult Login(LoginViewModel account);
        Task<IActionResult> Register(RegisterViewModel account);
        IEnumerable<CAccount> GetAccounts();
        CAccount GetAccountByUserName(string email);
        CAccount GetAccountById(string id);
        void AddNewAccount(CAccount acount);
        void DeleteAccountById(string id);
        void DeleteAccountByUserName(string email);
    }
}