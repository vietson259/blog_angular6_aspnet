﻿using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Security.Cryptography;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using Microsoft.IdentityModel.Tokens;
using MvcBlog.Data.Interfaces;
using MvcBlog.Data.ViewModel;
using MvcBlog.Models;

namespace MvcBlog.Data.Repositories
{
    public class AccountRepository : ControllerBase, IAccountRepository
    {
        private readonly MvcBlogContext _mvcBlogContext;

        public AccountRepository(MvcBlogContext mvcBlogContext)
        {
            _mvcBlogContext = mvcBlogContext;
        }

        public CAccount GetAccountById(string id)
        {
            return _mvcBlogContext.Account
                .FirstOrDefault(account => account.ID == int.Parse(id));
        }

        public IEnumerable<CAccount> GetAccounts()
        {
            return _mvcBlogContext.Account;
        }

        public CAccount GetAccountByUserName(string email)
        {
            return _mvcBlogContext.Account
                .FirstOrDefault(account =>
                string.Compare(account.UserName, email) == 0);
        }

        public void AddNewAccount(CAccount account)
        {
            _mvcBlogContext.Account.Add(account);
            _mvcBlogContext.SaveChanges();
        }

        public void DeleteAccountById(string id)
        {
            _mvcBlogContext.Account
                .Remove(_mvcBlogContext.Account
                .FirstOrDefault(account => account.ID == int.Parse(id)));
            _mvcBlogContext.SaveChanges();
        }

        public void DeleteAccountByUserName(string email)
        {
            _mvcBlogContext.Account
                .Remove(_mvcBlogContext.Account
                .FirstOrDefault(account => string.Compare(account.UserName, email) == 0));
            _mvcBlogContext.SaveChanges();
        }

        public async Task<IActionResult> Register(RegisterViewModel account)
        {
            if (!account.PassWord.Equals(account.Retyped, StringComparison.OrdinalIgnoreCase))
            {
                return BadRequest(error: "Retype not corect");
            }

            if (IsEmailExist(account.Email))
            {
                return BadRequest(error: "Email is exist");
            }

            CAccount newAccount = new CAccount
            {
                UserName = account.Email,
                PassWord = GetHashPassword(account.PassWord)
            };

            if (!ModelState.IsValid)
            {
                return BadRequest(error: ModelState.ValidationState.ToString());
            }

            await _mvcBlogContext.Account.AddAsync(newAccount);
            _mvcBlogContext.SaveChanges();

            return GetToken(account.Email);
        }

        private IActionResult GetToken(string email)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var signingKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes("API_BLOG_KEY_VERY_SECURITY_1234567890"));

            var token = new JwtSecurityToken(
                issuer: "https://securetoken.google.com/",
                audience: "https://securetoken.google.com/",
                expires: DateTime.UtcNow.AddHours(1),
                claims: claims,
                signingCredentials: new Microsoft.IdentityModel.Tokens
                    .SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            );

            return Ok(new
            {
                user = email,
                token = new JwtSecurityTokenHandler().WriteToken(token),
                expiration = token.ValidTo
            });
        }

        private bool IsEmailExist(string email)
        {
            var account = from acccount in _mvcBlogContext.Account
                          select acccount;
            if (account.Where(acc => acc.UserName == email).ToList().Count == 0)
            {
                return false;
            }
            return true;
        }

        private string GetHashPassword(string password)
        {
            var byteArray = new SHA1CryptoServiceProvider()
                                    .ComputeHash(Encoding.ASCII.GetBytes(password));
            StringBuilder hashPassword = new StringBuilder();
            for (int i = 0; i < byteArray.Length; i++)
            {
                hashPassword.Append(byteArray[i].ToString("x2"));
            }
            return hashPassword.ToString();
        }

        private bool IsLoggedSuccess(LoginViewModel account)
        {
            return _mvcBlogContext.Account.Any(user =>
                      user.UserName.Equals(account.Email, StringComparison.OrdinalIgnoreCase)
                      && user.PassWord.Equals(GetHashPassword(account.PassWord),
                                                StringComparison.OrdinalIgnoreCase));
        }

        public IActionResult Login(LoginViewModel account)
        {
            if (IsLoggedSuccess(account))
            {
                return GetToken(account.Email);
            }
            return Unauthorized();
        }
    }
}