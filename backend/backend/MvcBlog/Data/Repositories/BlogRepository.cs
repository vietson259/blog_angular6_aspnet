﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.EntityFrameworkCore;
using MvcBlog.Data.Interfaces;
using MvcBlog.Data.ViewModel;
using MvcBlog.Models;
using Slugify;

namespace MvcBlog.Data.Repositories
{
    public class BlogRepository : IBlogRepository
    {
        private readonly MvcBlogContext _mvcBlogContext;
        public BlogRepository(MvcBlogContext mvcBlogContext)
        {
            _mvcBlogContext = mvcBlogContext;
        }

        public IEnumerable<CPost> Blogs => _mvcBlogContext.Post.Include(blog => blog.Account)
                                .OrderByDescending(blog => blog.ViewdNumber)
                                .Take(4).ToList();

        private IEnumerable<CPost> GetBlogsByAccountID(int accountID)
        {
            var blogs = from blog in _mvcBlogContext.Post
                        .Include(blog => blog.Comments)
                        orderby blog.SubmittedDate ascending
                        select blog;
            return blogs.Where(blog => blog.AccountID == accountID);
        }

        //tra ve tat ca cac bai post hien thi tren trang index
        public IEnumerable<PostDetailViewModel> GetListAllPosts(int page)
        {
            var blogs = from blog in _mvcBlogContext.Post
                        .Include(blog => blog.Account)
                        orderby blog.SubmittedDate descending
                        select new PostDetailViewModel {
                            ID = blog.ID, IDAuthor = blog.Account.ID,
                            Title = blog.Title, MainContent = blog.MainContent,
                            Author = blog.Account.UserName, Avatar = blog.Account.Avatar,
                            Date = blog.SubmittedDate,
                            Slug = CreateSlug(blog.Title)
                        };
            return blogs.Skip(page * 6).Take(6).ToList();
        }
        private CPost GetBlogPageByPostID(int id)
        {
            var blog = from bg in _mvcBlogContext.Post
                       .Include(bg => bg.Comments)
                       .Include(bg => bg.Account)
                       orderby bg.SubmittedDate descending
                       select bg;
            return blog.Where(bg => bg.ID == id).First();
        }
        //lay du lieu bai post gom noi dung, comment, va recent post
        public PostDetailCellViewModel GetBlogDetailCellByIDPost(int id)
        {
            CPost blogTemp = GetBlogPageByPostID(id);
            return new PostDetailCellViewModel(blogTemp, 
                blogTemp.Comments, GetRecentPostBlogsByAccountID(blogTemp.AccountID).ToList());
        }

        private IEnumerable<CPost> GetRecentPostBlogsByAccountID(int id)
        {
            var blog = from bg in _mvcBlogContext.Post
                       orderby bg.ID descending
                       select bg;
            return blog.Where(bg => bg.AccountID == id).Take(4);
        }

        public void CreateBlog(CPost post)
        {
            //format blog properties for first
            post.ViewdNumber = 0;
            post.SubmittedDate = DateTime.Now;

            _mvcBlogContext.Post.Add(post);
            _mvcBlogContext.SaveChanges();
        }

        public void AddComment(CComment comment)
        {
            if (comment.AuthorName == null)
            {
                comment.AuthorName = "Anonymous";
            }
            comment.Date = DateTime.Now;

            _mvcBlogContext.Comment.Add(comment);
            _mvcBlogContext.SaveChanges();
        }

        public void DeleteBlog(string id)
        {
            if (id == null)
            {
                return;
            }
            var blog = _mvcBlogContext.Post.Find(int.Parse(id));
            _mvcBlogContext.Post.Remove(blog);
            _mvcBlogContext.SaveChanges();
        }

        public void CreatePost(PostViewModel post)
        {
            CPost cPost = new CPost
            {
                Title = post.Title,
                MainContent = post.MainContent,
                Content = post.Content,
                SubmittedDate = DateTime.Now,
                ViewdNumber = 0,
                AccountID = _GetIDAccountFromUsername(post.Username)
            };
            try
            {
                _mvcBlogContext.Post.Add(cPost);
                _mvcBlogContext.SaveChanges();
            }
            catch (Exception e)
            {
                throw (e);
            }
        }

        private int _GetIDAccountFromUsername(string username)
        {
            var id = from account in _mvcBlogContext.Account
                     where account.UserName == username
                     select account.ID;
            return id.First();
        }

        public IEnumerable<BlogDetailViewModel> 
            GetBlogDetailViewModelsByUserName(string username)
        {
            var posts = from post in _mvcBlogContext.Post
                        .Include(post => post.Account)
                        .Include(post => post.Comments)
                        orderby post.ID descending
                        select (new BlogDetailViewModel
                        {
                            UserName = post.Account.UserName,
                            Title = post.Title,
                            NumberOfComments = post.Comments.Count,
                            NumberOfViewd = post.ViewdNumber,
                            Date = post.SubmittedDate,
                            IDPost = post.ID,
                            Slug = CreateSlug(post.Title)
                        });
            return posts.Where(p => 
                p.UserName
                .Equals(username, StringComparison.Ordinal)).ToList();
        }
        private string CreateSlug(string title)
        {
            SlugHelper slug = new SlugHelper();
            return slug.GenerateSlug(title);
        }
        public void EditPost(PostViewModel post)
        {
            var cPost = _mvcBlogContext.Post.Find(post.ID);

            cPost.Content = post.Content;
            cPost.MainContent = post.MainContent;
            cPost.SubmittedDate = DateTime.Now;
            cPost.Title = post.Title;

            _mvcBlogContext.Post.Update(cPost);
            _mvcBlogContext.SaveChanges();
        }
    }
}