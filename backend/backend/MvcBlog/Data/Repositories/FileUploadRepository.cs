﻿using System;
using System.Drawing;

namespace MvcBlog.Data.Repositories
{
    public static class FileUploadRepository
    {
        public static int MAX_SIZE_PX = 600;
        public static int MIN_SIZE_PX = 120;
        private static bool IsLargerPX(string url, int defaultPX)
        {
            using (Image img = Image.FromFile(url))
            {
                if (img.Height >= defaultPX || img.Width >= defaultPX)
                    return true;
                return false;
            }
        }
        private static Bitmap _ScaleImage(string url, int maxSize)
        {
            double resizeFactor = 1;
            using (Image oldImage = Image.FromFile(url))
            {
                if (IsLargerPX(url, maxSize))
                {
                    double widthFactor = Convert.ToDouble(oldImage.Width) / maxSize;
                    double heightFactor = Convert.ToDouble(oldImage.Height) / maxSize;
                    resizeFactor = Math.Max(widthFactor, heightFactor);
                }

                int width = Convert.ToInt32(oldImage.Width / resizeFactor);
                int height = Convert.ToInt32(oldImage.Height / resizeFactor);

                Bitmap newImage = new Bitmap(width, height);
                using (Graphics g = Graphics.FromImage(newImage))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(oldImage, 0, 0, newImage.Width, newImage.Height);
                }
                return newImage;
            }            
        }
        private static void ScaleImage(string oldUrl, int maxSize, string newUrl)
        {
            using (Bitmap newImg = _ScaleImage(oldUrl, maxSize))
            {
                newImg.Save(newUrl);
            }         
        }
        private static Bitmap _CopyImage(string url)
        {
            using (Image oldImage = Image.FromFile(url))
            {
                int width = oldImage.Width;
                int height = oldImage.Height;
                Bitmap newImage = new Bitmap(width, height);
                using (Graphics g = Graphics.FromImage(newImage))
                {
                    g.InterpolationMode = System.Drawing.Drawing2D.InterpolationMode.HighQualityBicubic;
                    g.DrawImage(oldImage, 0, 0, newImage.Width, newImage.Height);
                    return newImage;
                }
            }
        }
        private static void CopyImage(string oldUrl, string newUrl)
        {
            using (Bitmap newImg = _CopyImage(oldUrl))
            {
                newImg.Save(newUrl);
            } 
        }
        public static void SaveImageAfterResize(string currentUrl, int defaultPX, string newUrl)
        {
            if (IsLargerPX(currentUrl, defaultPX))
            {
                ScaleImage(currentUrl, defaultPX, newUrl);
            } else
            {
                CopyImage(currentUrl, newUrl);
            }
        }

        public static void DeleteFile(string url)
        {
            if (System.IO.File.Exists(url))
            {
                // Use a try block to catch IOExceptions, to
                // handle the case of the file already being
                // opened by another process.
                try
                {
                    System.IO.File.Delete(url);
                }
                catch (System.IO.IOException e)
                {
                    throw (e);
                }
            }
        }
    }
}