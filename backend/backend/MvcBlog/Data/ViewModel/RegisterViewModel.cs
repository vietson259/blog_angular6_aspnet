﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.ViewModel
{
    public class RegisterViewModel
    {
        public string Email { get; set; }
        public string PassWord { get; set; }
        public string Retyped { get; set; }
    }
}