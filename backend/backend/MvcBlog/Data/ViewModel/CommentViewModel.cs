﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.ViewModel
{
    public class CommentViewModel
    {
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public int PostID { get; set; }
    }
}
