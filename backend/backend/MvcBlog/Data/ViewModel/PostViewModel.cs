﻿using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Data.ViewModel
{
    public class PostViewModel
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string MainContent { get; set; }
        public string Content { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int ViewdNumber { get; set; }
        public string Username { get; set; } //foreign key to Account table
    }
}