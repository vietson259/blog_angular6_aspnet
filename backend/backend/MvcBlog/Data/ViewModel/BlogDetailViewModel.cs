﻿using System;

namespace MvcBlog.Data.ViewModel
{
    public class BlogDetailViewModel
    {
        public string UserName { get; set; }
        public int IDPost { get; set; }
        public string Title { get; set; }
        public int NumberOfComments { get; set; }
        public int NumberOfViewd { get; set; }
        public DateTime Date { get; set; }
        public string Slug { get; set; }
    }
}
