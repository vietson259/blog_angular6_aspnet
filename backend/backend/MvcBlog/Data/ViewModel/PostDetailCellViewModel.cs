﻿using MvcBlog.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Slugify;


namespace MvcBlog.Data.ViewModel
{
    public class PostDetailCellViewModel
    {
        public Content Content { get; set; }
        public List<Comment> Comments { get; set; }
        public List<RecentPost> RecentPosts { get; set; }

        public PostDetailCellViewModel(CPost post, List<CComment> comments, 
            List<CPost> recentPosts)
        {
            Content = new Content
            {
                Author = post.Account.UserName,
                Title = post.Title,
                MainContent = post.MainContent,
                ContentPost = post.Content,
                Date = post.SubmittedDate,
                Avatar = post.Account.Avatar
            };

            Comments = new List<Comment>();
            RecentPosts = new List<RecentPost>();

            foreach(var item in comments)
            {
                Comments.Add(new Comment
                {
                    Author = item.AuthorName, Content = item.Content, Date = item.Date
                });
            }

            Comments.Reverse();

            foreach(var item in recentPosts)
            {
                RecentPosts.Add(new RecentPost
                {
                    ID = item.ID,
                    Title = item.Title,
                    Slug = CreateSlug(item.Title)
                });
            }
        }

        private string CreateSlug(string title)
        {
            SlugHelper slug = new SlugHelper();
            return slug.GenerateSlug(title);
        }
    }

    public class RecentPost
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string Slug { get; set; }
    }

    public class Comment
    {
        public string Author { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
    }

    public class Content
    {
        public string Author { get; set; }
        public string MainContent { get; set; }
        public string Title { get; set; }
        public DateTime Date { get; set; }
        public string ContentPost { get; set; }
        public string Avatar { get; set; }
    }
}