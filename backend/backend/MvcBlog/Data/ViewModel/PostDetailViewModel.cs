﻿using System;

namespace MvcBlog.Data.ViewModel
{
    public class PostDetailViewModel
    {
        public int ID { get; set; }
        public int IDAuthor { get; set; }
        public string Title { get; set; }
        public string MainContent { get; set; }
        public string Author { get; set; }
        public string Avatar { get; set; }
        public DateTime Date { get; set; }
        public string Slug { get; set; }
    }
}
