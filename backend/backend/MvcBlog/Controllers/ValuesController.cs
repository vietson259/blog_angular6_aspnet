﻿using System;
using System.Collections;
using System.Collections.Generic;
using System.IO;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using MvcBlog.Data.Repositories;

namespace MvcBlog.Controllers
{
    [Route("api/[controller]")]
    [ApiController]
    public class ValuesController : ControllerBase
    {
        private readonly IHostingEnvironment _hostingEnvironment;
        public ValuesController(IHostingEnvironment hostingEnvironment)
        {
            _hostingEnvironment = hostingEnvironment;
        }
        // GET api/values
        [HttpGet]
        [Authorize]
        public ActionResult<IEnumerable<string>> Get()
        {
            return new string[] { "value1", "value2" };
        }

        // GET api/values/5
        [HttpGet("{id}")]
        public ActionResult<string> Get(int id)
        {
            return "value";
        }

        //document from https://www.froala.com/wysiwyg-editor/docs/server/dotnet_core/image-upload
        [HttpPost]
        [Produces("application/json")]
        public async Task<IActionResult> Post(IFormFile files)
        {
            // Get the file from the POST request
            var theFile = HttpContext.Request.Form.Files.GetFile("image_param");

            // Get the server path, wwwroot
            string webRootPath = _hostingEnvironment.WebRootPath;

            // Building the path to the uploads directory
            var fileRoute = Path.Combine(webRootPath, "uploads");

            // Get the mime type
            var mimeType = theFile.ContentType;

            // Get File Extension
            string extension = System.IO.Path.GetExtension(theFile.FileName);

            // Generate Random name.
            string name = Guid.NewGuid().ToString().Substring(0, 8) + extension;

            // Build the full path inclunding the file name
            string link = Path.Combine(fileRoute, "");

            // Create directory if it does not exist.
            FileInfo dir = new FileInfo(fileRoute);
            dir.Directory.Create();

            // Basic validation on mime types and file extension
            string[] imageMimetypes = { "image/gif", "image/jpeg", "image/pjpeg", "image/x-png", "image/png", "image/svg+xml" };
            string[] imageExt = { ".gif", ".jpeg", ".jpg", ".png", ".svg", ".blob" };

            try
            {
                if (Array.IndexOf(imageMimetypes, mimeType) >= 0 && (Array.IndexOf(imageExt, extension) >= 0))
                {
                    // Copy contents to memory stream.
                    Stream stream;
                    stream = new MemoryStream();
                    theFile.CopyTo(stream);
                    stream.Position = 0;
                    string serverPath = link; 

                    //Save the file to folder uploads/temp
                    using (FileStream writerFileStream = System.IO.File.Create(serverPath + @"\temp\" + name))
                    {
                        await stream.CopyToAsync(writerFileStream);
                        writerFileStream.Dispose();
                    }                  

                    FileUploadRepository.SaveImageAfterResize(serverPath + @"\temp\" + name,
                                            FileUploadRepository.MAX_SIZE_PX, serverPath + @"\" + name);

                    FileUploadRepository.SaveImageAfterResize(serverPath + @"\temp\" + name,
                                            FileUploadRepository.MIN_SIZE_PX, serverPath + @"\thumbnail\" + name);

                    try
                    {
                        FileUploadRepository.DeleteFile(serverPath + @"\temp\" + name);
                    } catch (Exception e)
                    {
                        throw (e);
                    }

                    Hashtable imageUrl = new Hashtable();
                    imageUrl.Add("link", @"https://localhost:44332/uploads/" + name);
                    return Ok(imageUrl);
                }
                throw new ArgumentException("The image did not pass the validation");
            }
            catch (ArgumentException ex)
            {
                return BadRequest(ex.Message);
            }
        }
    }
}
