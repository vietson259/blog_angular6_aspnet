﻿using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Hosting;
using Microsoft.AspNetCore.Mvc;
using MvcBlog.Data.Interfaces;
using MvcBlog.Data.ViewModel;
using System;

namespace MvcBlog.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class BlogController : ControllerBase
    {
        private readonly IBlogRepository _blogRepository;
        private readonly IHostingEnvironment _hostingEnvironment;

        public BlogController(
            IBlogRepository blogRepository,
            IHostingEnvironment hostingEnvironment)
        {
            _blogRepository = blogRepository;
            _hostingEnvironment = hostingEnvironment;
        }

        [HttpGet]
        [Route("getListAllPosts/{pageNumber}")]
        public IActionResult GetListAllPosts(int pageNumber)
        {
            try
            {
                return Ok(_blogRepository.GetListAllPosts(pageNumber));
            } catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet]
        [Route("getPostDetail/{id}")]
        public IActionResult GetBlogsDetailCell(int id)
        {
            try
            {
                return Ok(_blogRepository.GetBlogDetailCellByIDPost(id));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost("comment")]
        public IActionResult PostComment([FromBody] CommentViewModel comment)
        {
            try
            {
                _blogRepository.AddComment(new Models.CComment
                {
                    AuthorName = comment.AuthorName,
                    Content = comment.Content,
                    Date = DateTime.Now,
                    PostID = comment.PostID
                });
                return Ok(Response.StatusCode);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPost("post")]
        [Authorize]
        public IActionResult Post([FromBody] PostViewModel post)
        {
            try
            {
                _blogRepository.CreatePost(post);
                return Ok(Response.StatusCode);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpPut("edit")]
        [Authorize]
        public IActionResult Edit([FromBody] PostViewModel post)
        {
            try
            {
                _blogRepository.EditPost(post);
                return Ok(Response.StatusCode);
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpGet]
        [Route("getBlogDetail/{username}")]
        [Authorize]
        public IActionResult GetBlogDetailByUserName(string username)
        {
            try
            {
                return Ok(_blogRepository.GetBlogDetailViewModelsByUserName(username));
            }
            catch (Exception e)
            {
                return BadRequest(e);
            }
        }

        [HttpDelete]
        [Route("deletePost/{idPost}")]
        [Authorize]
        public IActionResult DeletePost(string idPost)
        {
            try
            {
                _blogRepository.DeleteBlog(idPost);
                return Ok(Response.StatusCode);
            } catch(Exception e)
            {
                return BadRequest(e);
            }
        }
    }
}