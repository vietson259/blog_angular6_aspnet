﻿using System;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Mvc;
using MvcBlog.Data.Interfaces;
using System.Net;
using System.Net.Http;
using Microsoft.AspNetCore.Identity;
using MvcBlog.Data.ViewModel;
using System.IdentityModel.Tokens.Jwt;
using Microsoft.IdentityModel.Tokens;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authorization;

namespace MvcBlog.Controllers
{
    [Route("api/[controller]")]
    [Produces("application/json")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly UserManager<IdentityUser> _userManager;
        private readonly SignInManager<IdentityUser> _signInManager;
        private readonly IAccountRepository _accountRepository;

        public AccountController(
            IAccountRepository accountRepository,
            UserManager<IdentityUser> userManager,
            SignInManager<IdentityUser> signInManager)
        {
            _accountRepository = accountRepository;
            _userManager = userManager;
            _signInManager = signInManager;
        }

        private IActionResult ReturnResult(Object result)
        {
            if (result == null)
            {
                return NotFound(result);
            }
            return Ok(result);
        }

        private JwtSecurityToken GetToken(string email)
        {
            var claims = new[]
            {
                new Claim(JwtRegisteredClaimNames.Sub, email),
                new Claim(JwtRegisteredClaimNames.Jti, Guid.NewGuid().ToString())
            };

            var signingKey = new SymmetricSecurityKey(
                Encoding.UTF8.GetBytes("API_BLOG_KEY_VERY_SECURITY_1234567890"));

            return new JwtSecurityToken(
                issuer: "https://securetoken.google.com/",
                audience: "https://securetoken.google.com/",
                expires: DateTime.UtcNow.AddHours(1),
                claims: claims,
                signingCredentials: new Microsoft.IdentityModel.Tokens
                    .SigningCredentials(signingKey, SecurityAlgorithms.HmacSha256)
            );
        }

        [HttpPost]
        [Route("login")]
        public IActionResult Login([FromBody]LoginViewModel account)
        {
            return _accountRepository.Login(account);
        }

        [HttpGet]
        public IActionResult Get()
        {
            return ReturnResult(_accountRepository.GetAccounts().ToList());
        }

        [Authorize]
        [HttpGet("{id:int}")]
        public IActionResult GetAccountByID(string id)
        {
            return ReturnResult(_accountRepository.GetAccountById(id));
        }

        [Authorize]
        [HttpGet("GetAccountByEmail")]
        public IActionResult GetAccountByEmail([FromBody]string email)
        {
            return ReturnResult(_accountRepository.GetAccountByUserName(email));
        }

        [HttpPost]
        [Route("register")]
        public async Task<IActionResult> Register([FromBody] RegisterViewModel account)
        {
            return await _accountRepository.Register(account);
        }

        // DELETE api/<controller>/5
        [Authorize]
        [HttpDelete("{id:int}")]
        public HttpResponseMessage DeleteAccountByID(string id)
        {
            try
            {
                _accountRepository.DeleteAccountById(id);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return new HttpResponseMessage(HttpStatusCode.Created);
        }

        [Authorize]
        [HttpDelete("DeleteAccountByUserName/{email}")]
        public HttpResponseMessage DeleteAccountByUserName(string email)
        {
            try
            {
                _accountRepository.DeleteAccountByUserName(email);
            }
            catch (Exception)
            {
                return new HttpResponseMessage(HttpStatusCode.BadRequest);
            }
            return new HttpResponseMessage(HttpStatusCode.OK);
        }

        [HttpGet("checkLogin")]
        [Authorize]
        public IActionResult CheckLogin()
        {
            return Ok(HttpStatusCode.OK);
        }
    }
}