﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Models
{
    public class CComment
    {
        public int ID { get; set; }
        public string AuthorName { get; set; }
        public string Content { get; set; }
        public DateTime Date { get; set; }
        public int PostID { get; set; }
        public virtual CPost Post { get; set; }
    }
}
