﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Models
{
    public class CPost
    {
        public int ID { get; set; }
        public string Title { get; set; }
        public string MainContent { get; set; }
        public string Content { get; set; }
        public DateTime SubmittedDate { get; set; }
        public int ViewdNumber { get; set; }
        public int AccountID { get; set; } //foreign key to Account table
        public virtual CAccount Account { get; set; }
        public List<CComment> Comments { get; set; }
    }
}