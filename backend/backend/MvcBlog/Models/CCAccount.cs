﻿using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;

namespace MvcBlog.Models
{
    public class CAccount
    {
        public int ID { get; set; }
        [Required]
        [DataType(DataType.EmailAddress)]
        public string UserName { get; set; }
        [Required]
        [DataType(DataType.Password)]
        public string PassWord { get; set; }
        public string Avatar { get; set; }
        public List<CPost> Posts { get; set; }
        public List<CInfomation> Infomations { get; set; }
    }
}
