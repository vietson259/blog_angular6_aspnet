﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Models
{
    public class CInfomation
    {
        public int ID { get; set; }
        public string FirstName { get; set; }
        public string LastName { get; set; }
        public DateTime BirthDay { get; set; }
        public string Email { get; set; }
        public string AboutMe { get; set; }
        public int AccountID { get; set; } //foreign key to Account table
        public virtual CAccount Account { get; set; }
    }
}