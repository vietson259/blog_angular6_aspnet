﻿using Microsoft.EntityFrameworkCore;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace MvcBlog.Models
{
    public static class SeedData
    {
        public static void Initialize(MvcBlogContext context)
        {
            {
                //add data account table
                if (context.Account.Any()) return;
                var Accounts = new CAccount[] {
                    new CAccount
                    {
                        UserName = "kiencon"
                    },
                    new CAccount
                    {
                        UserName = "thon banh beo"
                    },
                    new CAccount
                    {
                        UserName = "thon tai tho"
                    },
                    new CAccount
                    {
                        UserName = "thon con nit"
                    },
                    new CAccount
                    {
                        UserName = "thon batiso"
                    },
                    new CAccount
                    {
                        UserName = "thon babe"
                    }
                };
                foreach (CAccount acc in Accounts)
                {
                    context.Account.Add(acc);
                }
                context.SaveChanges();

                //add data post table
                if (context.Post.Any()) return;
                var Postes = new CPost[] {
                    new CPost
                    {
                        Title = "BẠN TƯỞNG CSS ĐƠN GIẢN VÀ DỄ HỌC? BẠN SẼ NGHĨ LẠI SAU KHI ĐỌC BÀI VIẾT NÀY!",
                        Content = "Tuy vậy, khi trò chuyện với một số bạn học và làm về web, mình thấy các bạn có một suy nghĩ chung là: JavaScript thì khó thật, còn CSS dễ òm hà! CSS chỉ cần đặt class, id, sau đó viết file CSS là xong thôi.Nhiều bạn còn tự hào là mình tự học và thành thạo CSS trong vòng 1-2 tuần. CSS thật sự đơn giản và dễ học như vậy sao?Hãy cố gắng đọc hết bài viết này, bạn sẽ tự nhìn lại bản thân và suy nghĩ lại nhé!Thuở mới học web, mình cũng nghĩ HTML và CSS dễ òm hà! Thế nhưng, thuở bắt đầu làm việc với CSS trong dự án thật, bị thực tế vả cho sml, mình mới nhận ra hồi xưa mình ảo tưởng sức mạnh cỡ nào.Vì vậy, chúng ta sẽ cùng tìm hiểu xem, đằng sau vẻ ngoài đơn giản, CSS có gì làm anh em chúng ta đau đầu nhé.",
                        SubmittedDate = new DateTime(2000, 1, 1),
                        ViewdNumber = 4,
                        AccountID = 1,
                        MainContent = "Trong ngành lập trình Web, HTML/CSS/JavaScript là những thứ cực kì căn bản. Muốn làm một Web developer, ít nhiều gì ta cũng phải học và thành thạo những thứ trên."
                        //Account = _accountRepository.Accounts.First()
                    },
                    new CPost
                    {
                        Title = "DEPENDENCY INJECTION VÀ INVERSION OF CONTROL – PHẦN 1: ĐỊNH NGHĨA",
                        Content = "Hôm trước, có vài bạn nhờ mình giải thích các khái niệm Dependency Injection, Inversion of Control. Vốn lười, mình định google một bài viết bằng tiếng Việt để quăng cho các bạn ấy. Ngặc một nỗi là mình chả tìm được bài nào cụ thể rõ ràng về Dependency Injection, chỉ có hướng dẫn sử dụng Unity, StructureMap. Một số bài viết hay thì lại toàn bằng tiếng Anh. Mình cũng thấy vài bạn đã đi làm 1, 2 năm mà vẫn còn “ngáo ngơ” về DI, IoC, chỉ biết sử dụng nhưng không hiểu rõ bản chất của nó. Do đó, mình viết bài này nhằm giải thích một cách đơn giản dễ hiểu về Dependency Injection. Các bạn junior nên đọc thử, vì DI được áp dụng rất nhiều trong các ứng dụng doanh nghiệp, rất hay gặp khi đi làm/đi phỏng vấn. Pattern này được dùng trong cả C#, Java và các ngôn ngữ khác nên các bạn cứ đọc thoải mái nhé.Nhắc lại kiến thức Trước khi bắt đầu với Dependency Injection, các bạn hãy đọc lại bài viết về SOLID principles, những nguyên lý thiết kế và viết code. Nguyên lý cuối cùng trong SOLID chính là Dependency Inversion:1. Các module cấp cao không nên phụ thuộc vào các modules cấp thấp. Cả 2 nên phụ thuộc vào abstraction.2. Interface (abstraction) không nên phụ thuộc vào chi tiết, mà ngược lại. ( Các class giao tiếp với nhau thông qua interface, không phải thông qua implementation.)Với cách code thông thường, các module cấp cao sẽ gọi các module cấp thấp.Module cấp cao sẽ phụ thuộc và module cấp thấp, điều đó tạo ra các dependency. Khi module cấp thấp thay đổi, module cấp cao phải thay đổi theo.Một thay đổi sẽ kéo theo hàng loạt thay đổi, giảm khả năng bảo trì của code.",
                        SubmittedDate = new DateTime(2000, 7, 1),
                        ViewdNumber = 106,
                        AccountID = 1,
                        MainContent = "Khi nói tới DI, tức là nói tới Depedency Injection. Hiện nay, một số DI container như Unity, StructureMap v…v, hỗ trợ chúng ta trong việc cài đặt và áp dụng Dependency Injection vào code (Sẽ nói ở bài sau), tuy nhiên vẫn có thể gọi chúng là IoC Container, ý nghĩa tương tự nhau."
                        //Account = _accountRepository.Accounts.Last()
                    },
                    new CPost
                    {
                        Title = "SOLID LÀ GÌ – ÁP DỤNG CÁC NGUYÊN LÝ SOLID ĐỂ TRỞ THÀNH LẬP TRÌNH VIÊN CODE “CỨNG”",
                        Content = "Những khái niệm này đã được dạy khá rõ ràng, và hầu như những buổi phỏng vấn nào cũng có những câu hỏi liên quan đến khái niệm này. Vì 4 khái niệm này khá cơ bản, bạn nào chưa vũng có thể google để tìm hiểu thêm. Những nguyên lý mình giới thiệu hôm nay là những nguyên lý thiết kế trong OOP.Đây là những nguyên lý được đúc kết bởi máu xương vô số developer, rút ra từ hàng ngàn dự án thành công và thất bại.Một project áp dụng những nguyên lý này sẽ có code dễ đọc, dễ test, rõ ràng hơn.Và việc quan trọng nhất là việc maintainace code sẽ dễ hơn rất nhiều(Ai có kinh nghiệm trong ngành IT đều biết thời gian code chỉ chiếm 20 - 40 %, còn lại là thời gian để maintainance: thêm bớt chức năng và sửa lỗi).Nắm vững những nguyên lý này, đồng thời áp dụng chúng trong việc thiết kế + viết code sẽ giúp bạn tiến thêm 1 bước trên con đường thành senior nhé(1 ông senior bên FPT Software từng bảo mình thế). Class này giữ tới 3 trách nhiệm: Đọc dữ liệu từ DB, xử lý dữ liệu, in kết quả. Do đó, chỉ cần ta thay đổi DB, thay đổi cách xuất kết quả, … ta sẽ phải sửa đổi class này. Càng về sau class sẽ càng phình to ra. Theo đúng nguyên lý, ta phải tách class này ra làm 3 class riêng. Tuy số lượng class nhiều hơn những việc sửa chữa sẽ đơn giản hơn, class ngắn hơn nên cũng ít bug hơn.",
                        SubmittedDate = new DateTime(2000, 1, 1),
                        ViewdNumber = 40,
                        AccountID = 1,
                        MainContent = "Có thể thoải mái mở rộng 1 class, nhưng không được sửa đổi bên trong class đó (open for extension but closed for modification)."
                        //Account = _accountRepository.Accounts.Last()
                    },
                    new CPost
                    {
                        Title = "TỔNG HỢP NHỮNG CÂU NÓI GÂY ỨC CHẾ NHẤT CHO DEVELOPER",
                        Content = "Hôm nay, do không có hứng viết bài technical nên làm một bài dạng tổng hợp hài hước để anh em developer chúng mình vừa đọc vừa giải trí nhé. Trong bài này, chúng ta sẽ biết những câu nói … dễ gây ức chế nhất cho developer. Các bạn không theo ngành này thì xem cho biết để còn tránh.Các bạn theo ngành này thì nhớ share cho sếp và đồng nghiệp để họ hiểu và tránh nhé! Đi kèm với những câu nói “dễ gây ức chế” này,mình sẽ bình luận và giải thích lý do tại sao chúng lại khiến developer bực mình nha. Trong công việc Đa phần những câu nói “gây ức chế” chúng ta nghe được trong quá trình làm việc.Người nói thường là sếp, đồng nghiệp, khách hàng. Đây là câu của khách hàng hoặc sếp thường hay nói. Lý do là vì họ không hiểu được sự phức tạp của lập trình, nghĩ rằng chỉ cần gõ gõ vài tiếng là xong một chức năng. Trên thực tế, một chức năng nghe như đơn giản(thêm một textbox, một button, sửa lỗi “nhỏ”) đòi hỏi phải thay đổi code rất nhiều, từ UI cho tới Database.Việc này không hề “đơn giản”, cũng không thể  “chút” là xong được.",
                        SubmittedDate = new DateTime(2009, 12, 14),
                        ViewdNumber = 10,
                        AccountID = 1,
                        MainContent = "Không chỉ trong công việc, đôi khi developer chúng mình cũng nghe những câu nói rất dễ gây ức chế ngoài đời thật. Người nói thường là bạn bè, người quen hoặc … người xa lạ."
                        //Account = _accountRepository.Accounts.Last()
                    },
                    new CPost
                    {
                        Title = "LỰA CHỌN LAPTOP LẬP TRÌNH – NGƯỜI BẠN ĐỒNG HÀNH CỦA MỌI LẬP TRÌNH VIÊN",
                        Content = "Đối với một developer, laptop vô cùng quan trọng. nó là người bạn, người anh em đồng hành. Nó thứ bạn sử dụng hơn 8 tiếng mỗi ngày, vừa để học và làm việc, vừa để xem phim heo và vui chơi giải trí. Do vậy, trong bài viết này, mình sẽ chia sẻ một số tiêu chí để chọn laptop lập trình,cùng một số mẫu laptop phù hợp cho các bạn tham khảo nhé.Laptop nào mà chả lập trình được ? Sao phải chọn ? Đương nhiên là hầu như laptop nào cũng dùng để lập trình được, nhưng laptop nhanh sẽ làm tăng năng suất làm việc của bạn hơn.Đừng tiếc tiền mà dùng máy dỏm, vì một chiếc laptop tốt sẽ tiết kiệm của bạn rất nhiều thời gian và tiền bạc về sau. Giả sử mỗi ngày dùng laptop 8 - 10 tiếng, máy nhanh hơn 10 % sẽ giúp bạn tiết kiệm 1 tiếng mỗi ngày, 4 năm tiết kiệm được 1460 tiếng… tức 60 ngày(xem thêm về kĩ năng tính nhẩm “thần thánh” cho lập trình viên nhé). Chọn một chiếc laptop tốt ngay từ đầu cũng sẽ giúp bạn tiết kiệm được nhiều chi phí để nâng cấp, sữa chữa về sau.Chưa kể, nếu bạn muốn theo nghiệp lập trình game, lập trình di động thì phải có laptop mạnh hoặc chuyên biệt thì mới đáp ứng được nhu cầu.",
                        SubmittedDate = new DateTime(2017, 3, 1),
                        ViewdNumber = 10,
                        AccountID = 1,
                        MainContent = "Laptop lập trình quan trọng cỡ nào với lập trình viên?"//,
                        //Account = _accountRepository.Accounts.Last()
                    },
                    new CPost
                    {
                        Title = "Hàng chục nữ công nhân ngất vì rò rỉ khí gas",
                        Content = "Sáng 16/7, hàng trăm công nhân làm việc tại công ty may (Đài Loan) trên quốc lộ 13, phường Định Hòa, TP Thủ Dầu Một ngửi thấy mùi khí gas nồng nặc, gây khó thở. Họ hô hoán nhau bỏ chạy tán loạn, nhiều người bị ngất tại chỗ.Cảnh sát PCCC Bình Dương có mặt hiện trường, xác định khí gas bị rò rỉ từ đường ống của cơ sở sản xuất nước đá bên cạnh, sau đó hút vào hệ thống thông gió qua công ty may. Lính cứu hỏa đã đeo mặt nạ chống độc vào trong hiện trường khóa van bình gas.Theo cơ quan chức năng, có hơn 10 công nhân, đa phần là nữ được đưa đi cấp cứu. Đến trưa cùng ngày, sau khi được chăm sóc, họ đã được xuất viện.",
                        SubmittedDate = new DateTime(2013, 8, 1),
                        ViewdNumber = 101,
                        AccountID = 2,
                        MainContent = "Sáng 16/7, hàng trăm công nhân làm việc tại công ty may (Đài Loan) dasd asdsad asdas"
                    }
                };
                foreach (CPost post in Postes)
                {
                    context.Post.Add(post);
                }
                context.SaveChanges();

                //add data Infomation table
                if (context.Infomation.Any()) return;
                var Infomations = new CInfomation[] {
                    new CInfomation
                    {
                        FirstName = "Son",
                        LastName = "Bui Viet",
                        BirthDay = new DateTime(1989, 10, 16),
                        Email = "vietson1610@gmail.com",
                        AboutMe = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure sit, omnis labore quasi cumque dolorum id aut itaque similique corrupti ea, perferendis, mollitia, earum sed magnam fugiat qui amet illo!",
                        AccountID = 1
                    },
                    new CInfomation
                    {
                        FirstName = "thon",
                        LastName = "banh beo",
                        BirthDay = new DateTime(1997, 5, 12),
                        Email = "thonbanhbeo@gmail.com",
                        AboutMe = "Lorem ipsum dolor sit amet, consectetur adipisicing elit. Iure sit, omnis labore quasi cumque dolorum id aut itaque similique corrupti ea, perferendis, mollitia, earum sed magnam fugiat qui amet illo!",
                        AccountID = 2
                    }
                };
                foreach (CInfomation info in Infomations)
                {
                    context.Infomation.Add(info);
                }
                context.SaveChanges();

                //add data Comment table
                if (context.Comment.Any()) return;
                var Comments = new CComment[]{
                    new CComment
                    {
                        AuthorName = "Anoymous",
                        Content = "tks anh, bài viết rất bổ ích ạ. à mà hình như cái chuyện accessing css từ js có phải là ba cái thằng o function, f function với c function ko anh nhỉ?",
                        PostID = 2
                    },
                    new CComment
                    {
                        AuthorName = "Anoymous",
                        Content = "E làm dev web 2 năm nay, chưa bao giờ e coi CSS là đơn giản, là dễ học, nó cực kỳ gây ức chế và đau đầu, e thề e mà gặp thằng nào nói html/css dễ òm à, là e tát thẳng cho sml chứ ở đó mà dễ ",
                        PostID = 2
                    },
                    new CComment
                    {
                        AuthorName = "Anoymous",
                        Content = "E làm dev web 2 năm nay, chưa bao giờ e coi CSS là đơn giản, là dễ học, nó cực kỳ gây ức chế và đau đầu, e thề e mà gặp thằng nào nói html/css dễ òm à, là e tát thẳng cho sml chứ ở đó mà dễ ",
                        PostID = 3
                    }
                };
                foreach (CComment cm in Comments)
                {
                    context.Comment.Add(cm);
                }
                context.SaveChanges();
            }
        }
    }
}