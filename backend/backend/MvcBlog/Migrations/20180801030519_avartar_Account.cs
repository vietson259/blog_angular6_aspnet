﻿using Microsoft.EntityFrameworkCore.Migrations;

namespace MvcBlog.Migrations
{
    public partial class avartar_Account : Migration
    {
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.AlterColumn<string>(
                name: "PassWord",
                table: "Account",
                nullable: false,
                oldClrType: typeof(string),
                oldNullable: true);

            migrationBuilder.AddColumn<string>(
                name: "Avatar",
                table: "Account",
                nullable: true);
        }

        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DropColumn(
                name: "Avatar",
                table: "Account");

            migrationBuilder.AlterColumn<string>(
                name: "PassWord",
                table: "Account",
                nullable: true,
                oldClrType: typeof(string));
        }
    }
}
